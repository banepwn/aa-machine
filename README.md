# A JavaScript implementation of The Å-machine
This is a modified version of the official JavaScript implentation of [The Å-machine](http://www.linusakesson.net/dialog/aamachine/index.php). My modification changes how it's used slightly and removes the jQuery dependency, because I believe that *no one should be using jQuery in 2020.*

Modification originally done November 13-26, 2019. Not thoroughly tested. Not finished.
## Usage
1. Put the root directory somewhere on your web server
2. Put your base64 encoded story file in the root directory with the extension `.aastory.b64`
3. Go to <http://example.com/wherever/you/put/it/index.html?story=yourstoryhere>
## License
This was originally licensed under the BSD 2-clause license, so in compliance with the license terms I am also releasing this under the same license. There is a copy of the license terms in the LICENSE file.