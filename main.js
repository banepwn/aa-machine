"use strict";

var params = new URLSearchParams(location.search);
if (!params.has("story")) {
	throw "No story specified";
};
var story_raw = params.get("story");
var story_clean = story_raw.replace(/\W/g, "");
if (story_raw !== story_clean) {
	throw "Invalid URL";
};
var story_path = story_clean + ".aastory.b64";

var request = new XMLHttpRequest();
request.responseType = "blob";
request.addEventListener("readystatechange", function() {
	switch (request.readyState) {
		case 2:
			if (request.status !== 200) {
				throw "Received non-200 code when fetching story";
			};
			break;
		case 3:
			break;
		case 4:
			request.response.text()
			.then(function(data) {
				run_game(data.replace(/\n/g, ""), {});
			});
			break;
	};
});

request.open("get", story_path);
request.send();
